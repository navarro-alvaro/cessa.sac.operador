﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Operador.Presentacion
{
    public partial class FrmLogin : Form
    {
        private Negocio.Usuario usuarioNegocio;

        public FrmLogin()
        {
            InitializeComponent();
            this.AcceptButton = BtnIngresar;

            usuarioNegocio = new Negocio.Usuario();
            usuarioNegocio.MostrarInicio += new Negocio.Usuario.MostrarInicioEventHandler(usuario_MostrarInicio);
            usuarioNegocio.Alertas += new Negocio.Usuario.AlertasEventHandler(usuario_Alertas);
            usuarioNegocio.Errores += new Negocio.Usuario.ErroresEventHandler(usuario_Errores);
        }

        private void obtener_Version()
        {
            
        }

        void usuario_MostrarInicio()
        {
            var frmInicio = new FrmInicio(ref usuarioNegocio);
            frmInicio.Show();
            this.Visible = false;
        }

        void usuario_Alertas(string mensaje)
        {
            MessageBox.Show(mensaje, "Alerta");
        }

        void usuario_Errores(string mensaje)
        {
            MessageBox.Show(mensaje, "Ocurrió un error");
        }

        private void BtnIngresar_Click(object sender, EventArgs e)
        {
            usuarioNegocio.Login = TxtUsuario.Text;
            usuarioNegocio.Password = TxtContrasenia.Text;
            usuarioNegocio.LoginOK();
        }

        private void BtnSalir_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
