﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Operador.Presentacion
{
    public partial class FrmTicketsAtendidos : Form
    {
        private Negocio.Ticket ticketNegocio;
        private Negocio.TicketLista listaTicketsNegocio;
        private List<Object> tipos, estados, ubicaciones;

        public FrmTicketsAtendidos(ref Negocio.Ticket ticketNegocio)
        {
            InitializeComponent();

            this.ticketNegocio = ticketNegocio;
            this.listaTicketsNegocio = new Negocio.TicketLista();

            /** Tipos */
            tipos = new List<Object>();
            tipos.Add(new { Id = "", Value = "-" });
            tipos.Add(new { Id = "A", Value = "A" });
            tipos.Add(new { Id = "B", Value = "B" });
            tipos.Add(new { Id = "C", Value = "C" });
            tipos.Add(new { Id = "D", Value = "D" });
            tipos.Add(new { Id = "E", Value = "E" });
            tipos.Add(new { Id = "F", Value = "F" });
            tipos.Add(new { Id = "G", Value = "G" });
            tipos.Add(new { Id = "H", Value = "H" });
            tipos.Add(new { Id = "I", Value = "I" });
			CbxTipo.DataSource = tipos;
            CbxTipo.ValueMember = "Id";
            CbxTipo.DisplayMember = "Value";

            /** Ubicaciones */
            ubicaciones = ticketNegocio.getUbicaciones();
            CbxUbicacion.DataSource = ubicaciones;
            CbxUbicacion.ValueMember = "Id";
            CbxUbicacion.DisplayMember = "Value";

            /** Estados */
            estados = new List<Object>();
            estados.Add(new { Id = "", Value = "-----" });
            estados.Add(new { Id = "0", Value = "Solicitud" });
            estados.Add(new { Id = "1", Value = "En cola" });
            estados.Add(new { Id = "2", Value = "Llamado" });
            estados.Add(new { Id = "3", Value = "En atención" });
            estados.Add(new { Id = "4", Value = "Concluido" });
            estados.Add(new { Id = "5", Value = "Anulado" });
            CbxEstado.DataSource = estados;
            CbxEstado.ValueMember = "Id";
            CbxEstado.DisplayMember = "Value";
        }

        private void BtnBuscar_Click(object sender, EventArgs e)
        {
            List<Object> listaTickets = new List<Object>();
            listaTicketsNegocio.Buscar(
                CbxTipo.SelectedValue.ToString(), 
                TxtNumero.Text,
                DtpFecha.Value.Date.Year + "-" + DtpFecha.Value.Date.Month + "-" + DtpFecha.Value.Date.Day,
                CbxUbicacion.SelectedValue.ToString(),
                CbxEstado.SelectedValue.ToString(),
                ticketNegocio.UsuarioOperadorId
            );
            
            for (int i = 0; i < listaTicketsNegocio.Total; i++)
            {
                listaTickets.Add(new
                {
                    Ticket = listaTicketsNegocio[i].Tipo.ToString() + "-" + listaTicketsNegocio[i].Numero.ToString().PadLeft(3, '0'),
                    HoraSolicitud = listaTicketsNegocio[i].HoraSolicitud.ToString(),
                    HoraLlamada = listaTicketsNegocio[i].HoraLlamada.ToString(),
                    HoraAtencion = listaTicketsNegocio[i].HoraAtencion.ToString(),
                    HoraConclusion = listaTicketsNegocio[i].HoraConclusion.ToString(),
                    Ubicacion = listaTicketsNegocio[i].Ubicacion,
                    Estado = listaTicketsNegocio[i].Estado
                });
            }
            DtgvTickets.DataSource = listaTickets;

            /** Se cargan los datos en la barra de estado */
            List<String> estadisticas = ticketNegocio.getEstadisticas(
                CbxTipo.SelectedValue.ToString(),
                TxtNumero.Text,
                DtpFecha.Value.Date.Year + "-" + DtpFecha.Value.Date.Month.ToString().PadLeft(2, '0') + "-" + DtpFecha.Value.Date.Day.ToString().PadLeft(2, '0'),
                CbxUbicacion.SelectedValue.ToString(),
                CbxEstado.SelectedValue.ToString()
            );
            TStsTicketsAtendidos.Text = "Tickets Atendidos: " + estadisticas[0];
            TStsTicketsAnulados.Text = "Tickets Anulados: " + estadisticas[1];
            TStsTiempoPromedio.Text = "Tiempo Promedio de Atención: " + estadisticas[2];
        }
    }
}
