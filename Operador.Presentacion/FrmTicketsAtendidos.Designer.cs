﻿namespace Operador.Presentacion
{
    partial class FrmTicketsAtendidos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.DtgvTickets = new System.Windows.Forms.DataGridView();
            this.Ticket = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.HoraSolicitud = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.HoraLlamada = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.HoraAtencion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.HoraConclusion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Ubicacion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Estado = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GBxOpciones = new System.Windows.Forms.GroupBox();
            this.LblGuion = new System.Windows.Forms.Label();
            this.BtnBuscar = new System.Windows.Forms.Button();
            this.CbxEstado = new System.Windows.Forms.ComboBox();
            this.CbxUbicacion = new System.Windows.Forms.ComboBox();
            this.DtpFecha = new System.Windows.Forms.DateTimePicker();
            this.TxtNumero = new System.Windows.Forms.TextBox();
            this.CbxTipo = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.StsEstado = new System.Windows.Forms.StatusStrip();
            this.TStsTicketsAtendidos = new System.Windows.Forms.ToolStripStatusLabel();
            this.TStsTicketsAnulados = new System.Windows.Forms.ToolStripStatusLabel();
            this.TStsTiempoPromedio = new System.Windows.Forms.ToolStripStatusLabel();
            ((System.ComponentModel.ISupportInitialize)(this.DtgvTickets)).BeginInit();
            this.GBxOpciones.SuspendLayout();
            this.StsEstado.SuspendLayout();
            this.SuspendLayout();
            // 
            // DtgvTickets
            // 
            this.DtgvTickets.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DtgvTickets.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Ticket,
            this.HoraSolicitud,
            this.HoraLlamada,
            this.HoraAtencion,
            this.HoraConclusion,
            this.Ubicacion,
            this.Estado});
            this.DtgvTickets.Location = new System.Drawing.Point(12, 128);
            this.DtgvTickets.Name = "DtgvTickets";
            this.DtgvTickets.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DtgvTickets.Size = new System.Drawing.Size(760, 333);
            this.DtgvTickets.TabIndex = 0;
            // 
            // Ticket
            // 
            this.Ticket.DataPropertyName = "Ticket";
            this.Ticket.HeaderText = "Ticket";
            this.Ticket.Name = "Ticket";
            this.Ticket.ReadOnly = true;
            // 
            // HoraSolicitud
            // 
            this.HoraSolicitud.DataPropertyName = "HoraSolicitud";
            this.HoraSolicitud.HeaderText = "Hora de Solicitud";
            this.HoraSolicitud.Name = "HoraSolicitud";
            this.HoraSolicitud.ReadOnly = true;
            // 
            // HoraLlamada
            // 
            this.HoraLlamada.DataPropertyName = "HoraLlamada";
            this.HoraLlamada.HeaderText = "Hora de Llamada";
            this.HoraLlamada.Name = "HoraLlamada";
            this.HoraLlamada.ReadOnly = true;
            // 
            // HoraAtencion
            // 
            this.HoraAtencion.DataPropertyName = "HoraAtencion";
            this.HoraAtencion.HeaderText = "Hora de Atención";
            this.HoraAtencion.Name = "HoraAtencion";
            this.HoraAtencion.ReadOnly = true;
            // 
            // HoraConclusion
            // 
            this.HoraConclusion.DataPropertyName = "HoraConclusion";
            this.HoraConclusion.HeaderText = "Hora de Conclusión";
            this.HoraConclusion.Name = "HoraConclusion";
            this.HoraConclusion.ReadOnly = true;
            // 
            // Ubicacion
            // 
            this.Ubicacion.DataPropertyName = "Ubicacion";
            this.Ubicacion.HeaderText = "Ubicación";
            this.Ubicacion.Name = "Ubicacion";
            this.Ubicacion.ReadOnly = true;
            // 
            // Estado
            // 
            this.Estado.DataPropertyName = "Estado";
            this.Estado.HeaderText = "Estado";
            this.Estado.Name = "Estado";
            this.Estado.ReadOnly = true;
            // 
            // GBxOpciones
            // 
            this.GBxOpciones.Controls.Add(this.LblGuion);
            this.GBxOpciones.Controls.Add(this.BtnBuscar);
            this.GBxOpciones.Controls.Add(this.CbxEstado);
            this.GBxOpciones.Controls.Add(this.CbxUbicacion);
            this.GBxOpciones.Controls.Add(this.DtpFecha);
            this.GBxOpciones.Controls.Add(this.TxtNumero);
            this.GBxOpciones.Controls.Add(this.CbxTipo);
            this.GBxOpciones.Controls.Add(this.label4);
            this.GBxOpciones.Controls.Add(this.label3);
            this.GBxOpciones.Controls.Add(this.label2);
            this.GBxOpciones.Controls.Add(this.label1);
            this.GBxOpciones.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GBxOpciones.Location = new System.Drawing.Point(12, 12);
            this.GBxOpciones.Name = "GBxOpciones";
            this.GBxOpciones.Size = new System.Drawing.Size(760, 94);
            this.GBxOpciones.TabIndex = 1;
            this.GBxOpciones.TabStop = false;
            this.GBxOpciones.Text = "Búsqueda de Tickets.";
            // 
            // LblGuion
            // 
            this.LblGuion.AutoSize = true;
            this.LblGuion.Location = new System.Drawing.Point(62, 53);
            this.LblGuion.Name = "LblGuion";
            this.LblGuion.Size = new System.Drawing.Size(12, 14);
            this.LblGuion.TabIndex = 10;
            this.LblGuion.Text = "-";
            // 
            // BtnBuscar
            // 
            this.BtnBuscar.Location = new System.Drawing.Point(668, 48);
            this.BtnBuscar.Name = "BtnBuscar";
            this.BtnBuscar.Size = new System.Drawing.Size(75, 23);
            this.BtnBuscar.TabIndex = 9;
            this.BtnBuscar.Text = "Buscar";
            this.BtnBuscar.UseVisualStyleBackColor = true;
            this.BtnBuscar.Click += new System.EventHandler(this.BtnBuscar_Click);
            // 
            // CbxEstado
            // 
            this.CbxEstado.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CbxEstado.FormattingEnabled = true;
            this.CbxEstado.Location = new System.Drawing.Point(512, 49);
            this.CbxEstado.Name = "CbxEstado";
            this.CbxEstado.Size = new System.Drawing.Size(135, 22);
            this.CbxEstado.TabIndex = 8;
            // 
            // CbxUbicacion
            // 
            this.CbxUbicacion.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CbxUbicacion.FormattingEnabled = true;
            this.CbxUbicacion.Location = new System.Drawing.Point(338, 50);
            this.CbxUbicacion.Name = "CbxUbicacion";
            this.CbxUbicacion.Size = new System.Drawing.Size(135, 22);
            this.CbxUbicacion.TabIndex = 7;
            // 
            // DtpFecha
            // 
            this.DtpFecha.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.DtpFecha.Location = new System.Drawing.Point(187, 49);
            this.DtpFecha.MaxDate = new System.DateTime(3000, 12, 31, 0, 0, 0, 0);
            this.DtpFecha.MinDate = new System.DateTime(2015, 7, 1, 0, 0, 0, 0);
            this.DtpFecha.Name = "DtpFecha";
            this.DtpFecha.Size = new System.Drawing.Size(120, 22);
            this.DtpFecha.TabIndex = 6;
            // 
            // TxtNumero
            // 
            this.TxtNumero.Location = new System.Drawing.Point(75, 49);
            this.TxtNumero.Name = "TxtNumero";
            this.TxtNumero.Size = new System.Drawing.Size(69, 22);
            this.TxtNumero.TabIndex = 5;
            // 
            // CbxTipo
            // 
            this.CbxTipo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CbxTipo.FormattingEnabled = true;
            this.CbxTipo.Location = new System.Drawing.Point(23, 49);
            this.CbxTipo.Name = "CbxTipo";
            this.CbxTipo.Size = new System.Drawing.Size(38, 22);
            this.CbxTipo.TabIndex = 4;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(509, 29);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(49, 14);
            this.label4.TabIndex = 3;
            this.label4.Text = "Estado";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(335, 29);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(64, 14);
            this.label3.TabIndex = 2;
            this.label3.Text = "Ubicación";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(184, 29);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 14);
            this.label2.TabIndex = 1;
            this.label2.Text = "Fecha";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(20, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(54, 14);
            this.label1.TabIndex = 0;
            this.label1.Text = "Número";
            // 
            // StsEstado
            // 
            this.StsEstado.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.TStsTicketsAtendidos,
            this.TStsTicketsAnulados,
            this.TStsTiempoPromedio});
            this.StsEstado.Location = new System.Drawing.Point(0, 484);
            this.StsEstado.Name = "StsEstado";
            this.StsEstado.Size = new System.Drawing.Size(784, 22);
            this.StsEstado.SizingGrip = false;
            this.StsEstado.TabIndex = 2;
            // 
            // TStsTicketsAtendidos
            // 
            this.TStsTicketsAtendidos.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TStsTicketsAtendidos.Name = "TStsTicketsAtendidos";
            this.TStsTicketsAtendidos.Size = new System.Drawing.Size(128, 17);
            this.TStsTicketsAtendidos.Text = "Tickets Atendidos:";
            // 
            // TStsTicketsAnulados
            // 
            this.TStsTicketsAnulados.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TStsTicketsAnulados.Margin = new System.Windows.Forms.Padding(120, 3, 0, 2);
            this.TStsTicketsAnulados.Name = "TStsTicketsAnulados";
            this.TStsTicketsAnulados.Size = new System.Drawing.Size(122, 17);
            this.TStsTicketsAnulados.Text = "Tickets Anulados:";
            // 
            // TStsTiempoPromedio
            // 
            this.TStsTiempoPromedio.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TStsTiempoPromedio.Margin = new System.Windows.Forms.Padding(120, 3, 0, 2);
            this.TStsTiempoPromedio.Name = "TStsTiempoPromedio";
            this.TStsTiempoPromedio.Size = new System.Drawing.Size(204, 17);
            this.TStsTiempoPromedio.Text = "Tiempo Promedio de Atención:";
            // 
            // FrmTicketsAtendidos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 506);
            this.Controls.Add(this.StsEstado);
            this.Controls.Add(this.GBxOpciones);
            this.Controls.Add(this.DtgvTickets);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "FrmTicketsAtendidos";
            this.Text = "Tickets Atendidos";
            ((System.ComponentModel.ISupportInitialize)(this.DtgvTickets)).EndInit();
            this.GBxOpciones.ResumeLayout(false);
            this.GBxOpciones.PerformLayout();
            this.StsEstado.ResumeLayout(false);
            this.StsEstado.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView DtgvTickets;
        private System.Windows.Forms.GroupBox GBxOpciones;
        private System.Windows.Forms.ComboBox CbxEstado;
        private System.Windows.Forms.ComboBox CbxUbicacion;
        private System.Windows.Forms.DateTimePicker DtpFecha;
        private System.Windows.Forms.TextBox TxtNumero;
        private System.Windows.Forms.ComboBox CbxTipo;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button BtnBuscar;
        private System.Windows.Forms.DataGridViewTextBoxColumn Ticket;
        private System.Windows.Forms.DataGridViewTextBoxColumn HoraSolicitud;
        private System.Windows.Forms.DataGridViewTextBoxColumn HoraLlamada;
        private System.Windows.Forms.DataGridViewTextBoxColumn HoraAtencion;
        private System.Windows.Forms.DataGridViewTextBoxColumn HoraConclusion;
        private System.Windows.Forms.DataGridViewTextBoxColumn Ubicacion;
        private System.Windows.Forms.DataGridViewTextBoxColumn Estado;
        private System.Windows.Forms.Label LblGuion;
        private System.Windows.Forms.StatusStrip StsEstado;
        private System.Windows.Forms.ToolStripStatusLabel TStsTicketsAtendidos;
        private System.Windows.Forms.ToolStripStatusLabel TStsTicketsAnulados;
        private System.Windows.Forms.ToolStripStatusLabel TStsTiempoPromedio;
    }
}