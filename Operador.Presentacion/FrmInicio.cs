﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Operador.Presentacion
{
    public partial class FrmInicio : Form
    {
        Negocio.Ticket ticketNegocio;
        Negocio.Usuario usuarioNegocio;

        public FrmInicio(ref Negocio.Usuario usuarioNegocioForm)
        {
            InitializeComponent();

            usuarioNegocio = usuarioNegocioForm;
            ticketNegocio = new Negocio.Ticket();
            ticketNegocio.Alertas += new Negocio.Ticket.AlertasEventHandler(ticket_Alertas);
            ticketNegocio.Errores += new Negocio.Ticket.ErroresEventHandler(ticket_Errores);
            ticketNegocio.MostrarAtencion += new Negocio.Ticket.MostrarAtencionEventHandler(ticket_MostrarAtencion);

            LblUsuario.Text = "Usuario: " + usuarioNegocio.Nombre;
            LblRol.Text = "Rol: " + usuarioNegocio.Rol;
            LblUbicacion.Text = "Ubicación: ";
            if (usuarioNegocio.Rol == "CAJAS")
            {
                for (int i = 1; i <= 4; i++)
                    cmbUbicacion.Items.Add("Caja" + i);
            }
            else
            {
                if (usuarioNegocio.Rol == "PLATAFORMA")
                {
                    for (int i = 1; i <= 4; i++)
                        cmbUbicacion.Items.Add("PLT" + i);
                }
            }
            cmbUbicacion.SelectedItem = usuarioNegocio.Ubicacion;

            switch (usuarioNegocio.Rol)
            {
                case "CAJAS":
                    btnTransferir1.Text = "Transf. A Plataforma";
                    btnTransferir2.Text = "Transf. A Odeco";
                    break;
                case "PLATAFORMA":
                    btnTransferir1.Text = "Transf. A Cajas";
                    btnTransferir2.Text = "Transf. A Odeco";
                    break;
                case "ODECO":
                    btnTransferir1.Text = "Transf. A Cajas";
                    btnTransferir2.Text = "Transf. A Plataforma";
                    break;
            }

            ticketNegocio.UsuarioOperadorId = usuarioNegocio.Id;
            ticketNegocio.UsuarioRol = usuarioNegocio.Rol;
            ticketNegocio.UsuarioUbicacion = usuarioNegocio.Ubicacion;
            LblTotalTickets.Text = ticketNegocio.getTotalTicketsAtendidos().ToString();
            LblUltimoTicket.Text = ticketNegocio.getUltimoTicketAtendido();

			BtnLlamarTicket.Focus();
        }


        void ticket_MostrarAtencion()
        {
            this.Height = 580;
            GbxOpciones.Enabled = false;
            GbxTicket.Enabled = true;
            BtnAtenderConcluir.Text = "Atender";
            BtnCancelar.Enabled = true;
            BtnAnular.Enabled = true;
        }

        void ticket_OcultarAtencion()
        {
            /** Se vuelve a mostrar las opciones principales y se quitan las opciones del ticket */
            this.Height = 375;
            GbxOpciones.Enabled = true;
            GbxTicket.Enabled = false;
        }

        void ticket_Alertas(string mensaje)
        {
            MessageBox.Show(mensaje);
        }

        void ticket_Errores(string mensaje)
        {
            MessageBox.Show(mensaje, "Ocurrió un error");
        }

        private void BtnSalirSistema_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void FrmInicio_FormClosed(object sender, FormClosedEventArgs e)
        {
            usuarioNegocio.cerrarSesion();
            Application.Exit();
        }

        private void BtnLlamarTicket_Click(object sender, EventArgs e)
        {
            ticketNegocio.Llamar();

			BtnAtenderConcluir.Focus();

			LblNroTicket.Text = ticketNegocio.Tipo.ToString() + "-" + ticketNegocio.Numero.ToString().PadLeft(3, '0');
            if (ticketNegocio.Estado == "3")
            {
                BtnAtenderConcluir.Text = "Concluir";
                BtnCancelar.Enabled = false;
                BtnAnular.Enabled = false;
            }
        }

        private void BtnTicketsAtendidos_Click(object sender, EventArgs e)
        {
            var frmTicketsAtendidos = new FrmTicketsAtendidos(ref ticketNegocio);
            frmTicketsAtendidos.ShowDialog();
        }

        private void FrmInicio_Load(object sender, EventArgs e)
        {
            ticket_OcultarAtencion();
        }

        private void BtnAtenderConcluir_Click(object sender, EventArgs e)
        {
            if (BtnAtenderConcluir.Text == "Atender")
            {
                ticketNegocio.Atender();
                BtnAtenderConcluir.Text = "Concluir";
                BtnCancelar.Enabled = false;
                BtnAnular.Enabled = false;
                this.WindowState = FormWindowState.Minimized;
            }
            else
            {
                ticketNegocio.Concluir();
                LblTotalTickets.Text = ticketNegocio.getTotalTicketsAtendidos().ToString();
                LblUltimoTicket.Text = ticketNegocio.getUltimoTicketAtendido();

                ticket_OcultarAtencion();
            }
        }

        private void BtnCancelar_Click(object sender, EventArgs e)
        {
            ticketNegocio.Cancelar();

            /** Se vuelve a mostrar las opciones principales y se quitan las opciones del ticket */
            GbxOpciones.Enabled = true;
            this.Height = 375;
            GbxTicket.Enabled = false;
        }

        private void BtnAnular_Click(object sender, EventArgs e)
        {
            DialogResult anulacion = MessageBox.Show("¿Está seguro de eliminar el Ticket " + LblNroTicket.Text + "?",
                "Anulación de Ticket",
                MessageBoxButtons.YesNoCancel,
                MessageBoxIcon.Question,
                MessageBoxDefaultButton.Button2);
            if (anulacion == DialogResult.Yes)
            {
                ticketNegocio.Anular();
                ticket_OcultarAtencion();
            }
        }

        private void btnCambiarUbicacion_Click(object sender, EventArgs e)
        {
            DialogResult cambiarUbicacion = MessageBox.Show("¿Está seguro que desea cambiar su ubicación?", "Cambio de ubicación",
                MessageBoxButtons.YesNo,
                MessageBoxIcon.Question,
                MessageBoxDefaultButton.Button2);
            if (cambiarUbicacion == DialogResult.Yes)
            {
                usuarioNegocio.cambiarUbicacion(cmbUbicacion.SelectedItem.ToString());
                Application.Restart();
            }
        }

        private void btnTransferir1_Click(object sender, EventArgs e)
        {
            string tipo = "";
            switch (usuarioNegocio.Rol)
            {
                case "CAJAS":
                    tipo = "C-P";
                    break;
                case "PLATAFORMA":
                    tipo = "P-C";
                    break;
                case "ODECO":
                    tipo = "O-C";
                    break;
            }
            ticketNegocio.Transferir(tipo);

            /** Se vuelve a mostrar las opciones principales y se quitan las opciones del ticket */
            GbxOpciones.Enabled = true;
            this.Height = 375;
            GbxTicket.Enabled = false;
        }

        private void btnTransferir2_Click(object sender, EventArgs e)
        {
            string tipo = "";
            switch (usuarioNegocio.Rol)
            {
                case "CAJAS":
                    tipo = "C-O";
                    break;
                case "PLATAFORMA":
                    tipo = "P-O";
                    break;
                case "ODECO":
                    tipo = "O-P";
                    break;
            }
            ticketNegocio.Transferir(tipo);

            /** Se vuelve a mostrar las opciones principales y se quitan las opciones del ticket */
            GbxOpciones.Enabled = true;
            this.Height = 375;
            GbxTicket.Enabled = false;
        }
    }
}
