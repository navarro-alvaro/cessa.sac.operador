﻿namespace Operador.Presentacion
{
    partial class FrmTicket
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.LblNroTicket = new System.Windows.Forms.Label();
            this.BtnAtenderConcluir = new System.Windows.Forms.Button();
            this.BtnCancelar = new System.Windows.Forms.Button();
            this.BtnAnular = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // LblNroTicket
            // 
            this.LblNroTicket.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.LblNroTicket.AutoSize = true;
            this.LblNroTicket.Font = new System.Drawing.Font("Tahoma", 39.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblNroTicket.Location = new System.Drawing.Point(77, 24);
            this.LblNroTicket.Name = "LblNroTicket";
            this.LblNroTicket.Size = new System.Drawing.Size(189, 64);
            this.LblNroTicket.TabIndex = 0;
            this.LblNroTicket.Text = "A-001";
            this.LblNroTicket.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // BtnAtenderConcluir
            // 
            this.BtnAtenderConcluir.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnAtenderConcluir.Location = new System.Drawing.Point(10, 112);
            this.BtnAtenderConcluir.Name = "BtnAtenderConcluir";
            this.BtnAtenderConcluir.Size = new System.Drawing.Size(100, 30);
            this.BtnAtenderConcluir.TabIndex = 1;
            this.BtnAtenderConcluir.Text = "Atender";
            this.BtnAtenderConcluir.UseVisualStyleBackColor = true;
            this.BtnAtenderConcluir.Click += new System.EventHandler(this.BtnAtenderConcluir_Click);
            // 
            // BtnCancelar
            // 
            this.BtnCancelar.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancelar.Location = new System.Drawing.Point(120, 112);
            this.BtnCancelar.Name = "BtnCancelar";
            this.BtnCancelar.Size = new System.Drawing.Size(100, 30);
            this.BtnCancelar.TabIndex = 2;
            this.BtnCancelar.Text = "Cancelar";
            this.BtnCancelar.UseVisualStyleBackColor = true;
            this.BtnCancelar.Click += new System.EventHandler(this.BtnCancelar_Click);
            // 
            // BtnAnular
            // 
            this.BtnAnular.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnAnular.Location = new System.Drawing.Point(230, 112);
            this.BtnAnular.Name = "BtnAnular";
            this.BtnAnular.Size = new System.Drawing.Size(100, 30);
            this.BtnAnular.TabIndex = 3;
            this.BtnAnular.Text = "Anular";
            this.BtnAnular.UseVisualStyleBackColor = true;
            this.BtnAnular.Click += new System.EventHandler(this.BtnAnular_Click);
            // 
            // FrmTicket
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(341, 152);
            this.ControlBox = false;
            this.Controls.Add(this.BtnAnular);
            this.Controls.Add(this.BtnCancelar);
            this.Controls.Add(this.BtnAtenderConcluir);
            this.Controls.Add(this.LblNroTicket);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "FrmTicket";
            this.Text = "Ticket";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label LblNroTicket;
        private System.Windows.Forms.Button BtnAtenderConcluir;
        private System.Windows.Forms.Button BtnCancelar;
        private System.Windows.Forms.Button BtnAnular;
    }
}