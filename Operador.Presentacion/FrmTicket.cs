﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Operador.Presentacion
{
    public partial class FrmTicket : Form
    {
        Negocio.Ticket ticketNegocio;
        Label totalTickets;
        Label ultimoTicket;
        public FrmTicket(ref Negocio.Ticket ticket, ref Label LblTotalTickets, ref Label LblUltimoTicket)
        {
            InitializeComponent();

            ticketNegocio = ticket;
            LblNroTicket.Text = ticketNegocio.Tipo.ToString() + "-" + ticketNegocio.Numero.ToString().PadLeft(3, '0');
            totalTickets = LblTotalTickets;
            ultimoTicket = LblUltimoTicket;
            if (ticketNegocio.Estado == "3")
            {
                BtnAtenderConcluir.Text = "Concluir";
                BtnCancelar.Enabled = false;
                BtnAnular.Enabled = false;
            }
        }

        private void BtnAtenderConcluir_Click(object sender, EventArgs e)
        {
            if (BtnAtenderConcluir.Text == "Atender")
            {
                ticketNegocio.Atender();
                BtnAtenderConcluir.Text = "Concluir";
                BtnCancelar.Enabled = false;
                BtnAnular.Enabled = false;
            } 
            else 
            {
                ticketNegocio.Concluir();
                totalTickets.Text = ticketNegocio.getTotalTicketsAtendidos().ToString();
                ultimoTicket.Text = ticketNegocio.getUltimoTicketAtendido();
                this.Close();
            }
        }

        private void BtnCancelar_Click(object sender, EventArgs e)
        {
            ticketNegocio.Cancelar();
            this.Close();
        }

        private void BtnAnular_Click(object sender, EventArgs e)
        {
            DialogResult anulacion = MessageBox.Show("¿Está seguro de eliminar el Ticket " + LblNroTicket.Text + "?",
		        "Anulación de Ticket",
		        MessageBoxButtons.YesNoCancel,
		        MessageBoxIcon.Question,
		        MessageBoxDefaultButton.Button2);
            if (anulacion == DialogResult.Yes)
            {
                ticketNegocio.Anular();
                this.Close();
            }
        }

        
    }
}
