﻿namespace Operador.Presentacion
{
    partial class FrmInicio
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.LblTotalTickets = new System.Windows.Forms.Label();
            this.BtnLlamarTicket = new System.Windows.Forms.Button();
            this.BtnTicketsAtendidos = new System.Windows.Forms.Button();
            this.BtnSalirSistema = new System.Windows.Forms.Button();
            this.LblTotalTicketsLabel = new System.Windows.Forms.Label();
            this.LblUltimoTicketLabel = new System.Windows.Forms.Label();
            this.LblUltimoTicket = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.LblUsuario = new System.Windows.Forms.Label();
            this.GbxOpciones = new System.Windows.Forms.GroupBox();
            this.LblRol = new System.Windows.Forms.Label();
            this.LblUbicacion = new System.Windows.Forms.Label();
            this.PnlDatosUsuario = new System.Windows.Forms.Panel();
            this.btnCambiarUbicacion = new System.Windows.Forms.Button();
            this.cmbUbicacion = new System.Windows.Forms.ComboBox();
            this.GbxTicket = new System.Windows.Forms.GroupBox();
            this.btnTransferir2 = new System.Windows.Forms.Button();
            this.btnTransferir1 = new System.Windows.Forms.Button();
            this.BtnAnular = new System.Windows.Forms.Button();
            this.BtnCancelar = new System.Windows.Forms.Button();
            this.BtnAtenderConcluir = new System.Windows.Forms.Button();
            this.LblNroTicket = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.GbxOpciones.SuspendLayout();
            this.PnlDatosUsuario.SuspendLayout();
            this.GbxTicket.SuspendLayout();
            this.SuspendLayout();
            // 
            // LblTotalTickets
            // 
            this.LblTotalTickets.AutoSize = true;
            this.LblTotalTickets.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblTotalTickets.Location = new System.Drawing.Point(256, 20);
            this.LblTotalTickets.Name = "LblTotalTickets";
            this.LblTotalTickets.Size = new System.Drawing.Size(0, 19);
            this.LblTotalTickets.TabIndex = 0;
            // 
            // BtnLlamarTicket
            // 
            this.BtnLlamarTicket.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnLlamarTicket.Location = new System.Drawing.Point(19, 27);
            this.BtnLlamarTicket.Name = "BtnLlamarTicket";
            this.BtnLlamarTicket.Size = new System.Drawing.Size(120, 60);
            this.BtnLlamarTicket.TabIndex = 1;
            this.BtnLlamarTicket.Text = "Llamar Ticket";
            this.BtnLlamarTicket.UseVisualStyleBackColor = true;
            this.BtnLlamarTicket.Click += new System.EventHandler(this.BtnLlamarTicket_Click);
            // 
            // BtnTicketsAtendidos
            // 
            this.BtnTicketsAtendidos.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnTicketsAtendidos.Location = new System.Drawing.Point(165, 27);
            this.BtnTicketsAtendidos.Name = "BtnTicketsAtendidos";
            this.BtnTicketsAtendidos.Size = new System.Drawing.Size(120, 60);
            this.BtnTicketsAtendidos.TabIndex = 2;
            this.BtnTicketsAtendidos.Text = "Tickets Atendidos";
            this.BtnTicketsAtendidos.UseVisualStyleBackColor = true;
            this.BtnTicketsAtendidos.Click += new System.EventHandler(this.BtnTicketsAtendidos_Click);
            // 
            // BtnSalirSistema
            // 
            this.BtnSalirSistema.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSalirSistema.Location = new System.Drawing.Point(311, 27);
            this.BtnSalirSistema.Name = "BtnSalirSistema";
            this.BtnSalirSistema.Size = new System.Drawing.Size(120, 60);
            this.BtnSalirSistema.TabIndex = 3;
            this.BtnSalirSistema.Text = "Salir del Sistema";
            this.BtnSalirSistema.UseVisualStyleBackColor = true;
            this.BtnSalirSistema.Click += new System.EventHandler(this.BtnSalirSistema_Click);
            // 
            // LblTotalTicketsLabel
            // 
            this.LblTotalTicketsLabel.AutoSize = true;
            this.LblTotalTicketsLabel.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblTotalTicketsLabel.Location = new System.Drawing.Point(19, 20);
            this.LblTotalTicketsLabel.Name = "LblTotalTicketsLabel";
            this.LblTotalTicketsLabel.Size = new System.Drawing.Size(214, 19);
            this.LblTotalTicketsLabel.TabIndex = 4;
            this.LblTotalTicketsLabel.Text = "Total Tickets Atendidos: ";
            // 
            // LblUltimoTicketLabel
            // 
            this.LblUltimoTicketLabel.AutoSize = true;
            this.LblUltimoTicketLabel.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblUltimoTicketLabel.Location = new System.Drawing.Point(19, 58);
            this.LblUltimoTicketLabel.Name = "LblUltimoTicketLabel";
            this.LblUltimoTicketLabel.Size = new System.Drawing.Size(204, 19);
            this.LblUltimoTicketLabel.TabIndex = 5;
            this.LblUltimoTicketLabel.Text = "Último Ticket Atendido:";
            // 
            // LblUltimoTicket
            // 
            this.LblUltimoTicket.AutoSize = true;
            this.LblUltimoTicket.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblUltimoTicket.Location = new System.Drawing.Point(256, 58);
            this.LblUltimoTicket.Name = "LblUltimoTicket";
            this.LblUltimoTicket.Size = new System.Drawing.Size(0, 19);
            this.LblUltimoTicket.TabIndex = 6;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.LblTotalTicketsLabel);
            this.panel1.Controls.Add(this.LblUltimoTicket);
            this.panel1.Controls.Add(this.LblTotalTickets);
            this.panel1.Controls.Add(this.LblUltimoTicketLabel);
            this.panel1.Location = new System.Drawing.Point(19, 109);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(413, 100);
            this.panel1.TabIndex = 7;
            // 
            // LblUsuario
            // 
            this.LblUsuario.AutoSize = true;
            this.LblUsuario.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblUsuario.ForeColor = System.Drawing.Color.White;
            this.LblUsuario.Location = new System.Drawing.Point(16, 13);
            this.LblUsuario.Name = "LblUsuario";
            this.LblUsuario.Size = new System.Drawing.Size(61, 16);
            this.LblUsuario.TabIndex = 8;
            this.LblUsuario.Text = "Usuario:";
            // 
            // GbxOpciones
            // 
            this.GbxOpciones.Controls.Add(this.BtnTicketsAtendidos);
            this.GbxOpciones.Controls.Add(this.BtnLlamarTicket);
            this.GbxOpciones.Controls.Add(this.panel1);
            this.GbxOpciones.Controls.Add(this.BtnSalirSistema);
            this.GbxOpciones.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GbxOpciones.Location = new System.Drawing.Point(12, 97);
            this.GbxOpciones.Name = "GbxOpciones";
            this.GbxOpciones.Size = new System.Drawing.Size(449, 227);
            this.GbxOpciones.TabIndex = 9;
            this.GbxOpciones.TabStop = false;
            this.GbxOpciones.Text = "Opciones a realizar";
            // 
            // LblRol
            // 
            this.LblRol.AutoSize = true;
            this.LblRol.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblRol.ForeColor = System.Drawing.Color.White;
            this.LblRol.Location = new System.Drawing.Point(16, 42);
            this.LblRol.Name = "LblRol";
            this.LblRol.Size = new System.Drawing.Size(33, 16);
            this.LblRol.TabIndex = 10;
            this.LblRol.Text = "Rol:";
            // 
            // LblUbicacion
            // 
            this.LblUbicacion.AutoSize = true;
            this.LblUbicacion.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblUbicacion.ForeColor = System.Drawing.Color.White;
            this.LblUbicacion.Location = new System.Drawing.Point(276, 13);
            this.LblUbicacion.Name = "LblUbicacion";
            this.LblUbicacion.Size = new System.Drawing.Size(73, 16);
            this.LblUbicacion.TabIndex = 11;
            this.LblUbicacion.Text = "Ubicación:";
            // 
            // PnlDatosUsuario
            // 
            this.PnlDatosUsuario.BackColor = System.Drawing.Color.SteelBlue;
            this.PnlDatosUsuario.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PnlDatosUsuario.Controls.Add(this.btnCambiarUbicacion);
            this.PnlDatosUsuario.Controls.Add(this.cmbUbicacion);
            this.PnlDatosUsuario.Controls.Add(this.LblUbicacion);
            this.PnlDatosUsuario.Controls.Add(this.LblUsuario);
            this.PnlDatosUsuario.Controls.Add(this.LblRol);
            this.PnlDatosUsuario.Location = new System.Drawing.Point(12, 12);
            this.PnlDatosUsuario.Name = "PnlDatosUsuario";
            this.PnlDatosUsuario.Size = new System.Drawing.Size(446, 73);
            this.PnlDatosUsuario.TabIndex = 12;
            // 
            // btnCambiarUbicacion
            // 
            this.btnCambiarUbicacion.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCambiarUbicacion.Location = new System.Drawing.Point(279, 35);
            this.btnCambiarUbicacion.Name = "btnCambiarUbicacion";
            this.btnCambiarUbicacion.Size = new System.Drawing.Size(159, 30);
            this.btnCambiarUbicacion.TabIndex = 7;
            this.btnCambiarUbicacion.Text = "Cambiar Ubicacion";
            this.btnCambiarUbicacion.UseVisualStyleBackColor = true;
            this.btnCambiarUbicacion.Click += new System.EventHandler(this.btnCambiarUbicacion_Click);
            // 
            // cmbUbicacion
            // 
            this.cmbUbicacion.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbUbicacion.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.cmbUbicacion.FormattingEnabled = true;
            this.cmbUbicacion.Location = new System.Drawing.Point(355, 8);
            this.cmbUbicacion.Name = "cmbUbicacion";
            this.cmbUbicacion.Size = new System.Drawing.Size(83, 24);
            this.cmbUbicacion.TabIndex = 12;
            // 
            // GbxTicket
            // 
            this.GbxTicket.Controls.Add(this.btnTransferir2);
            this.GbxTicket.Controls.Add(this.btnTransferir1);
            this.GbxTicket.Controls.Add(this.BtnAnular);
            this.GbxTicket.Controls.Add(this.BtnCancelar);
            this.GbxTicket.Controls.Add(this.BtnAtenderConcluir);
            this.GbxTicket.Controls.Add(this.LblNroTicket);
            this.GbxTicket.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GbxTicket.Location = new System.Drawing.Point(12, 338);
            this.GbxTicket.Name = "GbxTicket";
            this.GbxTicket.Size = new System.Drawing.Size(450, 197);
            this.GbxTicket.TabIndex = 13;
            this.GbxTicket.TabStop = false;
            this.GbxTicket.Text = "Ticket";
            // 
            // btnTransferir2
            // 
            this.btnTransferir2.Location = new System.Drawing.Point(224, 147);
            this.btnTransferir2.Name = "btnTransferir2";
            this.btnTransferir2.Size = new System.Drawing.Size(160, 30);
            this.btnTransferir2.TabIndex = 9;
            this.btnTransferir2.Text = "button2";
            this.btnTransferir2.UseVisualStyleBackColor = true;
            this.btnTransferir2.Click += new System.EventHandler(this.btnTransferir2_Click);
            // 
            // btnTransferir1
            // 
            this.btnTransferir1.Location = new System.Drawing.Point(64, 147);
            this.btnTransferir1.Name = "btnTransferir1";
            this.btnTransferir1.Size = new System.Drawing.Size(154, 30);
            this.btnTransferir1.TabIndex = 8;
            this.btnTransferir1.Text = "button1";
            this.btnTransferir1.UseVisualStyleBackColor = true;
            this.btnTransferir1.Click += new System.EventHandler(this.btnTransferir1_Click);
            // 
            // BtnAnular
            // 
            this.BtnAnular.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnAnular.Location = new System.Drawing.Point(284, 111);
            this.BtnAnular.Name = "BtnAnular";
            this.BtnAnular.Size = new System.Drawing.Size(100, 30);
            this.BtnAnular.TabIndex = 7;
            this.BtnAnular.Text = "Anular";
            this.BtnAnular.UseVisualStyleBackColor = true;
            this.BtnAnular.Click += new System.EventHandler(this.BtnAnular_Click);
            // 
            // BtnCancelar
            // 
            this.BtnCancelar.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancelar.Location = new System.Drawing.Point(174, 111);
            this.BtnCancelar.Name = "BtnCancelar";
            this.BtnCancelar.Size = new System.Drawing.Size(100, 30);
            this.BtnCancelar.TabIndex = 6;
            this.BtnCancelar.Text = "Cancelar";
            this.BtnCancelar.UseVisualStyleBackColor = true;
            this.BtnCancelar.Click += new System.EventHandler(this.BtnCancelar_Click);
            // 
            // BtnAtenderConcluir
            // 
            this.BtnAtenderConcluir.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnAtenderConcluir.Location = new System.Drawing.Point(64, 111);
            this.BtnAtenderConcluir.Name = "BtnAtenderConcluir";
            this.BtnAtenderConcluir.Size = new System.Drawing.Size(100, 30);
            this.BtnAtenderConcluir.TabIndex = 5;
            this.BtnAtenderConcluir.Text = "Atender";
            this.BtnAtenderConcluir.UseVisualStyleBackColor = true;
            this.BtnAtenderConcluir.Click += new System.EventHandler(this.BtnAtenderConcluir_Click);
            // 
            // LblNroTicket
            // 
            this.LblNroTicket.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.LblNroTicket.AutoSize = true;
            this.LblNroTicket.Font = new System.Drawing.Font("Tahoma", 39.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblNroTicket.Location = new System.Drawing.Point(131, 29);
            this.LblNroTicket.Name = "LblNroTicket";
            this.LblNroTicket.Size = new System.Drawing.Size(189, 64);
            this.LblNroTicket.TabIndex = 4;
            this.LblNroTicket.Text = "A-001";
            this.LblNroTicket.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // FrmInicio
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(473, 541);
            this.Controls.Add(this.GbxTicket);
            this.Controls.Add(this.PnlDatosUsuario);
            this.Controls.Add(this.GbxOpciones);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "FrmInicio";
            this.Text = "Sistema de Atención al Cliente CESSA v1.1.0";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FrmInicio_FormClosed);
            this.Load += new System.EventHandler(this.FrmInicio_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.GbxOpciones.ResumeLayout(false);
            this.PnlDatosUsuario.ResumeLayout(false);
            this.PnlDatosUsuario.PerformLayout();
            this.GbxTicket.ResumeLayout(false);
            this.GbxTicket.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label LblTotalTickets;
        private System.Windows.Forms.Button BtnLlamarTicket;
        private System.Windows.Forms.Button BtnTicketsAtendidos;
        private System.Windows.Forms.Button BtnSalirSistema;
        private System.Windows.Forms.Label LblTotalTicketsLabel;
        private System.Windows.Forms.Label LblUltimoTicketLabel;
        private System.Windows.Forms.Label LblUltimoTicket;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label LblUsuario;
        private System.Windows.Forms.GroupBox GbxOpciones;
        private System.Windows.Forms.Label LblRol;
        private System.Windows.Forms.Label LblUbicacion;
        private System.Windows.Forms.Panel PnlDatosUsuario;
        private System.Windows.Forms.GroupBox GbxTicket;
        private System.Windows.Forms.Button BtnAnular;
        private System.Windows.Forms.Button BtnCancelar;
        private System.Windows.Forms.Button BtnAtenderConcluir;
        private System.Windows.Forms.Label LblNroTicket;
        private System.Windows.Forms.ComboBox cmbUbicacion;
        private System.Windows.Forms.Button btnCambiarUbicacion;
        private System.Windows.Forms.Button btnTransferir2;
        private System.Windows.Forms.Button btnTransferir1;
    }
}