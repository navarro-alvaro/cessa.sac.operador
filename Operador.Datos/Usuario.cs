﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Operador.Datos
{
    public class Usuario
    {
        public Usuario()
        {
            usuarioEnUso.Login = string.Empty;
            usuarioEnUso.Password = string.Empty;
            usuarioEnUso.Nombre = string.Empty;
            usuarioEnUso.Rol = string.Empty;
            usuarioEnUso.Ubicacion = string.Empty;
        }

        //Estructura para almacenar la información
        private struct TUsuario
        {
            public int Id;
            public string Login;
            public string Password;
            public string Nombre;
            public string Rol;
            public string Ubicacion;
        }
        TUsuario usuarioEnUso = new TUsuario();

        //Propiedades
        public int Id
        {
            get { return usuarioEnUso.Id; }
            set { usuarioEnUso.Id = value; }
        }
        public string Login
        {
            get { return usuarioEnUso.Login; }
            set { usuarioEnUso.Login = value; }
        }
        public string Password
        {
            get { return usuarioEnUso.Password; }
            set { usuarioEnUso.Password = value; }
        }
        public string Nombre
        {
            get { return usuarioEnUso.Nombre; }
            set { usuarioEnUso.Nombre = value; }
        }
        public string Rol
        {
            get { return usuarioEnUso.Rol; }
            set { usuarioEnUso.Rol = value; }
        }
        public string Ubicacion
        {
            get { return usuarioEnUso.Ubicacion; }
            set { usuarioEnUso.Ubicacion = value; }
        }

        //Métodos
        public bool LoginOK()
        {
            Librerias.DBMySQL dbMySQL = new Librerias.DBMySQL();
            try
            {
                string query = "SELECT id, login, nombre, rol, ubicacion FROM usuarios WHERE login = '" + usuarioEnUso.Login + "' AND password = '" + Librerias.SHA1Pass.GetSHA1(usuarioEnUso.Password) + "' AND estado = 1";
                MySqlDataReader dtrUsuario = dbMySQL.Seleccionar(query);
                if (dtrUsuario.Read())
                {
                    usuarioEnUso.Id = Convert.ToInt32(dtrUsuario["id"].ToString());
                    usuarioEnUso.Login = dtrUsuario["login"].ToString();
                    usuarioEnUso.Nombre = dtrUsuario["nombre"].ToString();
                    usuarioEnUso.Rol = dtrUsuario["rol"].ToString();
                    usuarioEnUso.Ubicacion = dtrUsuario["ubicacion"].ToString();
                    dtrUsuario.Close();
                    query = "INSERT INTO usuarios_log(fecha_ingreso, login, estado) VALUES (NOW(), '" + usuarioEnUso.Login + "', 'C')";
                    if (dbMySQL.Insertar(query))
                    {
                        dbMySQL.CerrarConexion();
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show("No puede conectar al servidor. Contacte al Administrador");
                return false;
            }
        }

        public void cambiarUbicacion(string ubicacion)
        {
            Librerias.DBMySQL dbMySQL = new Librerias.DBMySQL();
            string query = "UPDATE usuarios SET ubicacion = '" + ubicacion + "' WHERE id = " + usuarioEnUso.Id;
            dbMySQL.Actualizar(query);
        }

        public void cerrarSesion()
        {
            Librerias.DBMySQL dbMySQL = new Librerias.DBMySQL();
            string query = "UPDATE usuarios_log SET fecha_salida = NOW(), duracion = SEC_TO_TIME(TIMESTAMPDIFF(SECOND, fecha_ingreso, NOW())), estado = 'D' " +
                            "WHERE fecha_salida IS NULL AND login = '" + usuarioEnUso.Login + "' AND estado = 'C'";
            dbMySQL.Actualizar(query);
        }

        public void obtenerVersion()
        {
            Librerias.DBMySQL dbMySQL = new Librerias.DBMySQL();
            string query = "SELECT operador FROM version WHERE id = 1";
            dbMySQL.Seleccionar(query);
        }

    }
}
