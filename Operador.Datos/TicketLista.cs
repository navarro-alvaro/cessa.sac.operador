﻿using MySql.Data.MySqlClient;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Operador.Datos
{
    public class TicketLista
    {
        private List<Ticket> listaTickets;
        private Hashtable estados;

        public TicketLista()
        {
            listaTickets = new List<Ticket>();
            estados = new Hashtable();
            estados.Add("0", "Solicitud");
            estados.Add("1", "En Cola");
            estados.Add("2", "Llamado");
            estados.Add("3", "En atención");
            estados.Add("4", "Concluido");
            estados.Add("5", "Anulado");
        }

        // Propiedades
        public Ticket this[int index]
        {
            get
            {
                return listaTickets[index];
            }
            set
            {
                listaTickets[index] = value;
            }
        }

        public List<Ticket> Lista
        {
            get { return listaTickets; }
        }

        public int Total
        {
            get { return listaTickets.Count(); }
        }

        public void Buscar(string tipo, string numero, string fecha, string ubicacion, string estado, int usuario_operador_id)
        {
            Librerias.DBMySQL dbMySQL = new Librerias.DBMySQL();
            string query = "SELECT tipo, numero, fecha_solicitud, hora_solicitud, hora_llamada, hora_atencion, hora_conclusion, estado, ubicacion " + 
                " FROM tickets " +
                " WHERE usuario_operador_id = " + usuario_operador_id.ToString();
            if (!string.IsNullOrEmpty(tipo)) query += " AND tipo LIKE '" + tipo + "%' ";
            if (!string.IsNullOrEmpty(numero)) query += " AND numero = " + numero;
            if (!string.IsNullOrEmpty(fecha)) query += " AND fecha_solicitud = '" + fecha + "' ";
            if (!string.IsNullOrEmpty(ubicacion)) query += " AND ubicacion = '" + ubicacion + "' ";
            if (!string.IsNullOrEmpty(estado)) query += " AND estado = '" + estado + "' ";
            query += " ORDER BY estado ASC, fecha_solicitud DESC, hora_atencion DESC";

            MySqlDataReader dtrTickets = dbMySQL.Seleccionar(query);
            listaTickets.Clear();
            while (dtrTickets.Read())
            {
                listaTickets.Add(new Ticket
                {
                    Tipo = dtrTickets["tipo"].ToString(),
                    Numero = Convert.ToInt32(dtrTickets["numero"]),
                    FechaSolicitud = dtrTickets["fecha_solicitud"].ToString(),
                    HoraSolicitud = dtrTickets["hora_solicitud"].ToString(),
                    HoraLlamada = dtrTickets["hora_llamada"].ToString(),
                    HoraAtencion = dtrTickets["hora_atencion"].ToString(),
                    HoraConclusion = dtrTickets["hora_conclusion"].ToString(),
                    Estado = estados[dtrTickets["estado"].ToString()].ToString(),
                    Ubicacion = dtrTickets["ubicacion"].ToString()
                });
            }
            dbMySQL.CerrarConexion();
        }

    }
}
