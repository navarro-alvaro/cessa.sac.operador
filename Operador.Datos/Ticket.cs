﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Operador.Datos
{
    public class Ticket
    {

		public Ticket()
        {
            ticketEnUso.Tipo = string.Empty;
            ticketEnUso.Numero = 0;
            ticketEnUso.FechaSolicitud = string.Empty;
            ticketEnUso.HoraSolicitud = string.Empty;
            ticketEnUso.HoraAtencion = string.Empty;
            ticketEnUso.UsuarioOperadorId = 0;
            ticketEnUso.UsuarioRol = string.Empty;
            ticketEnUso.UsuarioUbicacion = string.Empty;
        }

        //Estructura para almacenar la información
        private struct TTicket
        {
            public int Id;
            public string Tipo;
            public int Numero;
            public string FechaSolicitud;
            public string HoraSolicitud;
            public string HoraLlamada;
            public string HoraAtencion;
            public string HoraConclusion;
            public string Estado;
            public string Ubicacion;
            public int UsuarioOperadorId;
            public string UsuarioRol;
            public string UsuarioUbicacion;
        }
        TTicket ticketEnUso = new TTicket();

        //Propiedades
        public int Id
        {
            get { return ticketEnUso.Id; }
            set { ticketEnUso.Id = value; }
        }
        public string Tipo
        {
            get { return ticketEnUso.Tipo; }
            set { ticketEnUso.Tipo = value; }
        }
        public int Numero
        {
            get { return ticketEnUso.Numero; }
            set { ticketEnUso.Numero = value; }
        }
        public string FechaSolicitud
        {
            get { return ticketEnUso.FechaSolicitud; }
            set { ticketEnUso.FechaSolicitud = value; }
        }
        public string HoraSolicitud
        {
            get { return ticketEnUso.HoraSolicitud; }
            set { ticketEnUso.HoraSolicitud = value; }
        }
        public string HoraLlamada
        {
            get { return ticketEnUso.HoraLlamada; }
            set { ticketEnUso.HoraLlamada = value; }
        }
        public string HoraAtencion
        {
            get { return ticketEnUso.HoraAtencion; }
            set { ticketEnUso.HoraAtencion = value; }
        }
        public string HoraConclusion
        {
            get { return ticketEnUso.HoraConclusion; }
            set { ticketEnUso.HoraConclusion = value; }
        }
        public string Estado
        {
            get { return ticketEnUso.Estado; }
            set { ticketEnUso.Estado = value; }
        }
        public string Ubicacion
        {
            get { return ticketEnUso.Ubicacion; }
            set { ticketEnUso.Ubicacion = value; }
        }
        public int UsuarioOperadorId
        {
            get { return ticketEnUso.UsuarioOperadorId; }
            set { ticketEnUso.UsuarioOperadorId = value; }
        }
        public string UsuarioRol
        {
            get { return ticketEnUso.UsuarioRol; }
            set { ticketEnUso.UsuarioRol = value; }
        }
        public string UsuarioUbicacion
        {
            get { return ticketEnUso.UsuarioUbicacion; }
            set { ticketEnUso.UsuarioUbicacion = value; }
        }

        //Métodos
        public bool Llamar()
        {
            Librerias.DBMySQL dbMySQL = new Librerias.DBMySQL();
            /** Verifica si hay ticket en estado 2=Llamado o 3=En atención */
            string query = "SELECT id, tipo, numero, fecha_solicitud, hora_solicitud, hora_atencion, estado FROM tickets WHERE fecha_solicitud = CURRENT_DATE AND estado IN ('2', '3')";
            switch (ticketEnUso.UsuarioRol)
            {
				case "PLATAFORMA":
					query += " AND tipo IN ('A', 'B', 'C') AND usuario_operador_id = " + ticketEnUso.UsuarioOperadorId + " ORDER BY estado DESC, tipo DESC, numero ASC";
					break;
				case "ODECO":
					query += " AND tipo IN ('D', 'E', 'F') AND usuario_operador_id = " + ticketEnUso.UsuarioOperadorId + " ORDER BY estado DESC, tipo DESC, numero ASC";
					break;
				case "CAJAS":
					query += " AND tipo IN ('G', 'H', 'I') AND usuario_operador_id = " + ticketEnUso.UsuarioOperadorId + " ORDER BY estado DESC, tipo DESC, numero ASC";
					break;
            }
            MySqlDataReader dtrTicket = dbMySQL.Seleccionar(query);
            if (dtrTicket.Read())
            {
                ticketEnUso.Id = Convert.ToInt32(dtrTicket["id"].ToString());
                ticketEnUso.Tipo = dtrTicket["tipo"].ToString();
                ticketEnUso.Numero = Convert.ToInt32(dtrTicket["numero"].ToString());
                ticketEnUso.FechaSolicitud = dtrTicket["fecha_solicitud"].ToString();
                ticketEnUso.HoraSolicitud = dtrTicket["hora_solicitud"].ToString();
                ticketEnUso.HoraAtencion = dtrTicket["hora_atencion"].ToString();
                ticketEnUso.Estado = dtrTicket["estado"].ToString();
                dtrTicket.Close();
                dbMySQL.CerrarConexion();
                return true;
            }
            else
            {
                dtrTicket.Close();
                /** Busca el último ticket en cola o solicitado */
                query = "SELECT id, hora_solicitud FROM tickets WHERE fecha_solicitud = CURRENT_DATE AND (estado < '2')";
                switch (ticketEnUso.UsuarioRol)
                {
                    case "PLATAFORMA":
                        
                        //Método alternativo
                        query = "SELECT `cessa_bdsacc`.`func_atencion_plataforma`()";
                        dtrTicket = dbMySQL.Seleccionar(query);
                        if (dtrTicket.Read())
                        {
                            query = dtrTicket.GetString(0);
                        }
                        dtrTicket.Close();
                        
                        break;
                    case "ODECO":
                        query += " AND tipo IN ('D', 'E', 'F') ORDER BY estado DESC, tipo DESC, numero ASC";
                        break;
                    case "CAJAS":
                        
                        //Método alternativo
                        query = "SELECT `cessa_bdsacc`.`func_atencion_cajas`()";
                        dtrTicket = dbMySQL.Seleccionar(query);
                        if (dtrTicket.Read())
                        {
                            query = dtrTicket.GetString(0);
                        }
                        dtrTicket.Close();

                        break;
                }
                //MessageBox.Show(query);
                dtrTicket = dbMySQL.Seleccionar(query);
                if (dtrTicket.Read())
                {
                    /** Hace un llamado a un ticket */
                    ticketEnUso.Id = Convert.ToInt32(dtrTicket["id"].ToString());
                    string query_update = "UPDATE tickets SET hora_llamada = CURRENT_TIME, estado = '2', ubicacion = '" + UsuarioUbicacion + "', usuario_operador_id = " + UsuarioOperadorId + " WHERE id = " + ticketEnUso.Id.ToString();
                    dtrTicket.Close();
                    dbMySQL.Actualizar(query_update);

                    query_update = "SELECT id, tipo, numero, fecha_solicitud, hora_solicitud, hora_atencion FROM tickets WHERE id = " + ticketEnUso.Id.ToString();
                    MySqlDataReader dtrTicketNuevo = dbMySQL.Seleccionar(query_update);
                    if (dtrTicketNuevo.Read())
                    {
                        ticketEnUso.Id = Convert.ToInt32(dtrTicketNuevo["id"].ToString());
                        ticketEnUso.Tipo = dtrTicketNuevo["tipo"].ToString();
                        ticketEnUso.Numero = Convert.ToInt32(dtrTicketNuevo["numero"].ToString());
                        ticketEnUso.FechaSolicitud = dtrTicketNuevo["fecha_solicitud"].ToString();
                        ticketEnUso.HoraSolicitud = dtrTicketNuevo["hora_solicitud"].ToString();
                        ticketEnUso.HoraAtencion = dtrTicketNuevo["hora_atencion"].ToString();
                        ticketEnUso.Estado = "1";
                        dtrTicketNuevo.Close();
                    }
                    dtrTicket.Close();
                    dbMySQL.CerrarConexion();
                    return true;
                }
                else
                {
                    dbMySQL.CerrarConexion();
                    return false;
                }
            }
        }

        public void Atender()
        {
            Librerias.DBMySQL dbMySQL = new Librerias.DBMySQL();
            string query = "CALL sp_atender(" + ticketEnUso.Id.ToString() + ", '" + ticketEnUso.Tipo.ToString() + "', '" + ticketEnUso.UsuarioUbicacion.ToString() + "', " + ticketEnUso.UsuarioOperadorId.ToString() + ")";
            dbMySQL.Ejecutar(query);
        }

        public void Concluir()
        {
            Librerias.DBMySQL dbMySQL = new Librerias.DBMySQL();
            string query = "UPDATE tickets SET hora_conclusion = CURTIME(), estado = '4' WHERE id = " + ticketEnUso.Id.ToString() + " AND estado = '3'";
            dbMySQL.Actualizar(query);
        }

        public void Cancelar()
        {
            Librerias.DBMySQL dbMySQL = new Librerias.DBMySQL();
            string query = "UPDATE tickets SET hora_llamada = NULL, estado = '1', ubicacion = NULL, usuario_operador_id = NULL WHERE id = " + ticketEnUso.Id.ToString() + " AND estado = '2'";
            dbMySQL.Actualizar(query);
        }

        public void Anular()
        {
            Librerias.DBMySQL dbMySQL = new Librerias.DBMySQL();
            string query = "UPDATE tickets SET hora_anulacion = CURTIME(), estado = '5' WHERE id = " + ticketEnUso.Id.ToString() + " AND estado = '2'";
            dbMySQL.Actualizar(query);
        }

        public void Transferir(string tipo)
        {
            Librerias.DBMySQL dbMySQL = new Librerias.DBMySQL();
            string query = "UPDATE tickets SET hora_llamada = NULL, hora_atencion = NULL, ";
            switch (tipo)
            {
                case "P-C":
                    query += "estado = '6', ";
                    break;
                case "O-C":
                    query += "estado = '7', ";
                    break;
                case "C-P":
                    query += "estado = '8', ";
                    break;
                case "O-P":
                    query += "estado = '9', ";
                    break;
                case "C-O":
                    query += "estado = '10', ";
                    break;
                case "P-O":
                    query += "estado = '11', ";
                    break;
            }
            query += "ubicacion = NULL, usuario_operador_id = NULL WHERE id = " + ticketEnUso.Id.ToString() + " AND(estado = '2' OR estado = '3')";
            dbMySQL.Actualizar(query);
        }

        public int getTotalTicketsAtendidos()
        {
            Librerias.DBMySQL dbMySQL = new Librerias.DBMySQL();
            string query = "SELECT COUNT(id) AS total FROM tickets WHERE fecha_solicitud = CURRENT_DATE AND usuario_operador_id = " + ticketEnUso.UsuarioOperadorId.ToString() + " AND estado > '3'";
            int result;
            MySqlDataReader dtrTotal = dbMySQL.Seleccionar(query);
            if (dtrTotal.Read())
            {
                result = Convert.ToInt32(dtrTotal["total"].ToString());
            }
            else
            {
                result = 0;
            }
            dbMySQL.CerrarConexion();
            return result;
        }

        public string getUltimoTicketAtendido()
        {
            Librerias.DBMySQL dbMySQL = new Librerias.DBMySQL();
            string query = "SELECT tipo, numero FROM tickets WHERE fecha_solicitud = CURRENT_DATE AND usuario_operador_id = " + ticketEnUso.UsuarioOperadorId.ToString() + " AND estado > '3' ORDER BY hora_conclusion DESC LIMIT 1 ";
            string result;
            MySqlDataReader dtrUltimo = dbMySQL.Seleccionar(query);
            if (dtrUltimo.Read())
            {
                result = dtrUltimo["tipo"].ToString() + "-" + dtrUltimo["numero"].ToString().PadLeft(3, '0');
            }
            else {
                result = "----";
            }
            dbMySQL.CerrarConexion();
            return result;
        }

        public List<Object> getUbicaciones()
        {
            Librerias.DBMySQL dbMySQL = new Librerias.DBMySQL();
            string query = "SELECT DISTINCT(ubicacion) FROM tickets WHERE ubicacion <> '' AND usuario_operador_id = " + ticketEnUso.UsuarioOperadorId.ToString() + " ORDER BY ubicacion ASC";
            List<Object> result = new List<Object>();
            MySqlDataReader dtrUbicaciones = dbMySQL.Seleccionar(query);
            result.Add(new {Id = "", Value = "-----"});
            while (dtrUbicaciones.Read())
            {
                result.Add(new { Id = dtrUbicaciones["ubicacion"].ToString(), Value = dtrUbicaciones["ubicacion"].ToString() });
            }
            dbMySQL.CerrarConexion();
            return result;
        }

        /** Obtiene los datos estadísticos de la atención de clientes */
        public List<String> getEstadisticas(string tipo, string numero, string fecha, string ubicacion, string estado)
        {
            Librerias.DBMySQL dbMySQL = new Librerias.DBMySQL();
            string query = "SELECT "
                + " COALESCE(SUM(CASE estado WHEN '4' THEN 1 ELSE 0 END), 0) AS concluidos, "
                + " COALESCE(SUM(CASE estado WHEN '5' THEN 1 ELSE 0 END), 0) AS anulados, "
                + " COALESCE(AVG(CASE estado WHEN '4' THEN (hora_conclusion - hora_atencion) END), 0) AS tiempo_promedio "
                + " FROM tickets "
                + " WHERE usuario_operador_id = " + ticketEnUso.UsuarioOperadorId.ToString();

            if (!string.IsNullOrEmpty(tipo)) query += " AND tipo LIKE '" + tipo + "%' ";
            if (!string.IsNullOrEmpty(numero)) query += " AND numero = " + numero;
            if (!string.IsNullOrEmpty(fecha)) query += " AND fecha_solicitud = '" + fecha + "' ";
            if (!string.IsNullOrEmpty(ubicacion)) query += " AND ubicacion = '" + ubicacion + "' ";
            if (!string.IsNullOrEmpty(estado)) query += " AND estado = '" + estado + "' ";

            List<String> result = new List<String>();
            MySqlDataReader dtrTotal = dbMySQL.Seleccionar(query);
            if (dtrTotal.Read())
            {
                result.Add(dtrTotal["concluidos"].ToString());
                result.Add(dtrTotal["anulados"].ToString());
                result.Add(Math.Round(float.Parse(dtrTotal["tiempo_promedio"].ToString()), 2).ToString());
            }
            dbMySQL.CerrarConexion();
            return result;
        }
    }
}
