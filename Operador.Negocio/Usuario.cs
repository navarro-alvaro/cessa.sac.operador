﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Operador.Negocio
{
    public class Usuario
    {
        public event MostrarInicioEventHandler MostrarInicio;
        public delegate void MostrarInicioEventHandler();
        public event AlertasEventHandler Alertas;
        public delegate void AlertasEventHandler(string mensaje);
        public event ErroresEventHandler Errores;
        public delegate void ErroresEventHandler(string mensaje);

        //Instancia de Datos para recuperar información
        Datos.Usuario usuarioDato;
        public Usuario()
        {
            usuarioDato = new Datos.Usuario();
        }

        //Propiedades
        public int Id
        {
            get { return usuarioDato.Id; }
            set { usuarioDato.Id = value; }
        }
        public string Login
        {
            get { return usuarioDato.Login; }
            set { usuarioDato.Login = value; }
        }
        public string Password
        {
            get { return usuarioDato.Password; }
            set { usuarioDato.Password = value; }
        }
        public string Nombre
        {
            get { return usuarioDato.Nombre; }
            set { usuarioDato.Nombre = value; }
        }
        public string Rol
        {
            get { return usuarioDato.Rol; }
            set { usuarioDato.Rol = value; }
        }
        public string Ubicacion
        {
            get { return usuarioDato.Ubicacion; }
            set { usuarioDato.Ubicacion = value; }
        }

        //Métodos
        public void LoginOK()
        {
            if (usuarioDato.Login.ToString().Trim() == "" || usuarioDato.Password.ToString().Trim() == "")
            {
                if (Alertas != null) Alertas("Debe escribir el Usuario y Contraseña");
            }
            else
            {
                if (usuarioDato.LoginOK())
                {
                    MostrarInicio();
                }
                else
                {
                    if (Alertas != null) Alertas("El Usuario o Contraseña no son los correctos");
                }
            }
        }

        public void cambiarUbicacion(string ubicacion)
        {
            usuarioDato.cambiarUbicacion(ubicacion);
            if (Alertas != null) Alertas("Cambió su ubicación correctamente.\nEs necesario reiniciar la aplicación.");
        }

        public void cerrarSesion()
        {
            usuarioDato.cerrarSesion();
        }
    }
}
