﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Operador.Negocio
{
    public class Ticket
    {
        public event MostrarAtencionEventHandler MostrarAtencion;
        public delegate void MostrarAtencionEventHandler();
        public event AlertasEventHandler Alertas;
        public delegate void AlertasEventHandler(string mensaje);
        public event ErroresEventHandler Errores;
        public delegate void ErroresEventHandler(string mensaje);

        //Instancia de Datos para recuperar información
        Datos.Ticket ticketDato;
        public Ticket()
        {
            ticketDato = new Datos.Ticket();
        }

        //Propiedades
        public string Tipo
        {
            get { return ticketDato.Tipo; }
            set { ticketDato.Tipo = value; }
        }
        public int Numero
        {
            get { return ticketDato.Numero; }
            set { ticketDato.Numero = value; }
        }
        public string FechaSolicitud
        {
            get { return ticketDato.FechaSolicitud; }
            set { ticketDato.FechaSolicitud = value; }
        }
        public string HoraSolicitud
        {
            get { return ticketDato.HoraSolicitud; }
            set { ticketDato.HoraSolicitud = value; }
        }
        public string HoraLlamada
        {
            get { return ticketDato.HoraLlamada; }
            set { ticketDato.HoraLlamada = value; }
        }
        public string HoraAtencion
        {
            get { return ticketDato.HoraAtencion; }
            set { ticketDato.HoraAtencion = value; }
        }
        public string HoraConclusion
        {
            get { return ticketDato.HoraConclusion; }
            set { ticketDato.HoraConclusion = value; }
        }
        public string Estado
        {
            get { return ticketDato.Estado; }
            set { ticketDato.Estado = value; }
        }
        public string Ubicacion
        {
            get { return ticketDato.Ubicacion; }
            set { ticketDato.Ubicacion = value; }
        }
        public int UsuarioOperadorId
        {
            get { return ticketDato.UsuarioOperadorId; }
            set { ticketDato.UsuarioOperadorId = value; }
        }
        public string UsuarioRol
        {
            get { return ticketDato.UsuarioRol; }
            set { ticketDato.UsuarioRol = value; }
        }
        public string UsuarioUbicacion
        {
            get { return ticketDato.UsuarioUbicacion; }
            set { ticketDato.UsuarioUbicacion = value; }
        }

        //Métodos
        public void Llamar()
        {
            if (ticketDato.Llamar())
            {
                MostrarAtencion();
            }
            else
            {
                if (Alertas != null) Alertas("No existe ningún Ticket pendiente");
            }
        }
        public void Atender()
        {
            ticketDato.Atender();
        }
        public void Concluir()
        {
            ticketDato.Concluir();
        }
        public void Cancelar()
        {
            ticketDato.Cancelar();
        }
        public void Anular()
        {
            ticketDato.Anular();
        }
        public void Transferir(string tipo)
        {
            ticketDato.Transferir(tipo);
        }
        public int getTotalTicketsAtendidos()
        {
            return ticketDato.getTotalTicketsAtendidos();
        }
        public string getUltimoTicketAtendido()
        {
            return ticketDato.getUltimoTicketAtendido();
        }
        public List<Object> getUbicaciones()
        {
            return ticketDato.getUbicaciones();
        }
        public List<String> getEstadisticas(string tipo, string numero, string fecha, string ubicacion, string estado)
        {
            return ticketDato.getEstadisticas(tipo, numero, fecha, ubicacion, estado);
        }
    }
}
