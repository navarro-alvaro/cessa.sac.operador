﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Operador.Negocio
{
    public class TicketLista
    {
        public event MostrarPantallaEventHandler MostrarPantalla;
        public delegate void MostrarPantallaEventHandler();
        public event ErroresEventHandler Errores;
        public delegate void ErroresEventHandler(string mensaje);

        private List<Ticket> listaTickets;

        public TicketLista()
        {
            listaTickets = new List<Ticket>();
        }

        public Ticket this[int index]
        {
            get
            {
                return listaTickets[index];
            }
            set
            {
                listaTickets[index] = value;
            }
        }

        public List<Ticket> Lista
        {
            get { return listaTickets; }
        }

        public int Total
        {
            get { return listaTickets.Count(); }
        }

        public void Buscar(string tipo, string numero, string fecha, string ubicacion, string estado, int usuario_operador_id)
        {
            Datos.TicketLista ticketListaDatos = new Datos.TicketLista();
            ticketListaDatos.Buscar(tipo, numero, fecha, ubicacion, estado, usuario_operador_id);
            listaTickets.Clear();
            for (int i = 0; i < ticketListaDatos.Total; i++)
            {
                listaTickets.Add(new Ticket
                {
                    Tipo = ticketListaDatos[i].Tipo,
                    Numero = ticketListaDatos[i].Numero,
                    FechaSolicitud = ticketListaDatos[i].FechaSolicitud,
                    HoraSolicitud = ticketListaDatos[i].HoraSolicitud,
                    HoraLlamada = ticketListaDatos[i].HoraLlamada,
                    HoraAtencion = ticketListaDatos[i].HoraAtencion,
                    HoraConclusion = ticketListaDatos[i].HoraConclusion,
                    Estado = ticketListaDatos[i].Estado,
                    Ubicacion = ticketListaDatos[i].Ubicacion
                });
            }
        }
    }
}
